
#pragma once

#include "MicroDST/ICloner.h"

// Forward declarations
namespace LHCb 
{
  class ProtoParticle;
  class Particle;
}

/** @class ICloneProtoParticle MicroDST/ICloneProtoParticle.h
 *  
 *
 *  @author Juan PALACIOS
 *  @date   2007-12-05
 */
class GAUDI_API ICloneProtoParticle : virtual public MicroDST::ICloner<LHCb::ProtoParticle>
{

public: 

  /// Interface ID
  DeclareInterfaceID(ICloneProtoParticle, 3, 0 );

  /// Destructor
  virtual ~ICloneProtoParticle() = default;

public:

  /// Clone the given ProtoParticle
  virtual LHCb::ProtoParticle* clone( const LHCb::ProtoParticle* protoParticle,
                                      const LHCb::Particle * parent = nullptr ) = 0;
  
};
