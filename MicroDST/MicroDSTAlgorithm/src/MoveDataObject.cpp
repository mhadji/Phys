// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h"
#include <GaudiKernel/DataObject.h>
#include <GaudiKernel/RegistryEntry.h>
// local
#include "MoveDataObject.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MoveDataObject
//
// 2010-09-28 : Juan Palacios
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MoveDataObject::MoveDataObject( const std::string& name,
                                ISvcLocator* pSvcLocator)
  : MicroDSTAlgorithm ( name , pSvcLocator )
//  , m_outputTESLocation  ( )
{ 
}
//=============================================================================
// Destructor
//=============================================================================
MoveDataObject::~MoveDataObject() {}
//=============================================================================
// Main execution
//=============================================================================
StatusCode MoveDataObject::execute()
{

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  typedef std::vector<std::string>::const_iterator stringIter;
  stringIter iLoc = this->inputTESLocations().begin();
  stringIter locEnd = this->inputTESLocations().end();


  for (; iLoc != locEnd; ++iLoc) {

    const std::string inputLocation = niceLocationName(*iLoc);

    executeLocation(inputLocation);

  }

  return StatusCode::SUCCESS;
}

//=============================================================================
void MoveDataObject::executeLocation(const std::string& objectLocation)
{

  std::string outputLocation = m_outputTESLocation;
  if ( outputLocation == "" ) 
    outputLocation = this->outputTESLocation( objectLocation );

  verbose() << "Going to move DataObject from " << objectLocation
            << " to " << outputLocation << endmsg;

  typedef KeyedContainer < KeyedObject<int> , Containers::HashMap > GenericContainer;
  ObjectContainerBase* pObj = 0;

  StatusCode sc = eventSvc()->retrieveObject(objectLocation,
                                             (DataObject*&)pObj);

  if (sc.isFailure() ) {
    Error("Failed to retrieve DataObject from "+
          objectLocation,
          StatusCode::FAILURE, 0).ignore();
    return;
  }



  GenericContainer* targetObj = 0;
  sc = eventSvc()->findObject(outputLocation,
                              (DataObject*&) targetObj);

  bool emptyDestination = !sc.isSuccess();

  if ( pObj )  
  {
    sc = eventSvc()->unregisterObject(pObj);
    if ( !sc.isSuccess() ) 
    {
      Error("Failed to unregister DataObject from "+
            objectLocation, 
            StatusCode::FAILURE, 0).ignore();

      return;
    }

    if (outputLocation == "KILL")
    {
      verbose() << "Killed location " << objectLocation << endmsg;
      return ;
    }

    if (emptyDestination)
    {
      sc = eventSvc()->registerObject(outputLocation, pObj);
      if (sc.isFailure())
      {
        Error("Could not register the new object in " + outputLocation,
                StatusCode::FAILURE, 0).ignore();
      }
    }
    else if (targetObj)
    {
      Warning("Non-empty destination while copying " 
            + objectLocation + " "
            + m_outputTESLocation
          );

      GenericContainer* pObjAsKeyed = static_cast<GenericContainer*> (pObj);
      if (pObjAsKeyed)
      {
        GenericContainer::iterator obj;

        std::vector<int> existingKeys; 
        for ( obj = targetObj->begin(); obj != targetObj->end(); ++obj)
          existingKeys . push_back ( (*obj)->key() ) ;

        for ( obj = pObjAsKeyed->begin(); obj != pObjAsKeyed->end(); ++obj)
        {
          const int key = (*obj)->key();
          for (int eKey : existingKeys )
            if ( key == eKey )
            {
              Error ( "Trying to merge KeyedContainers including "
                "objects with the same key.",  StatusCode::FAILURE, 0).ignore();
              return;
            }

          (*obj)->setParent ( targetObj );
          targetObj->insert ( *obj );
        }

        sc = eventSvc()->updateObject ( targetObj );
        if (sc.isFailure())
          Error("Failed to update the content of " + outputLocation, 
                 StatusCode::FAILURE, 0).ignore();
      }
      else
        return;
    }
    else
    {
      if (emptyDestination)
        Error("Failed to move the object from "+
              objectLocation+ " to " + outputLocation,
              StatusCode::FAILURE, 0).ignore();
      else
        Error("Failed to merge objects from "+
              objectLocation+ " and " + outputLocation,
              StatusCode::FAILURE, 0).ignore();

      return;
    }

    pObj->release();
  }

}
//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( MoveDataObject )
