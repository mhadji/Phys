// $Id: $
// Include files 

// from Gaudi
#include "GaudiKernel/ToolFactory.h" 

// local
#include "ObjectClonerBase.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ObjectClonerBase
//
// 2012-03-30 : Chris Jones
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ObjectClonerBase::ObjectClonerBase( const std::string& type,
                                    const std::string& name,
                                    const IInterface* parent )
  : MicroDSTTool ( type, name, parent )
{
  declareProperty("TESVetoList",m_tesVetoList);
  declareProperty("TESAlwaysClone",m_tesAlwaysClone);
}

//=============================================================================

StatusCode ObjectClonerBase::initialize()
{
  const auto sc = MicroDSTTool::initialize();
  if ( sc.isFailure() ) return sc;

  // sort for find efficiency
  std::sort( m_tesVetoList.begin(),    m_tesVetoList.end()    );
  std::sort( m_tesAlwaysClone.begin(), m_tesAlwaysClone.end() );

  return sc;
}

//=============================================================================
