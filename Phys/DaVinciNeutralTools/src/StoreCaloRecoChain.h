#ifndef STORECALORECOCHAIN_H 
#define STORECALORECOCHAIN_H 1

// Include files 
// from DaVinci.
#include "Kernel/DaVinciAlgorithm.h"
#include <memory>
#include "Event/Particle.h"
#include "CaloUtils/CaloParticle.h"
#include "CaloDAQ/ICaloDataProvider.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloInterfaces/ICounterLevel.h"

namespace StoreCaloUtils
{
  class DigitFromCalo 
  {
  public:
    explicit DigitFromCalo( const int  calo ): m_calo( calo ) {}
    explicit DigitFromCalo( const std::string& calo ): DigitFromCalo( CaloCellCode::CaloNumFromName( calo ) ) {}
    inline bool operator() ( const LHCb::CaloDigit* digit ) const
    {
      return digit && ( ( (int) digit->cellID().calo() ) == m_calo );
    }
    DigitFromCalo() = delete;
  private:
    int m_calo{0} ;
  };
  class Counter
  {
  public:
    Counter() = default;
    inline void set(){m_counters.clear();}
    inline void operator() (std::string&& object){m_counters[object] += 1;}
    inline void print()
    {
      for ( const auto c : m_counters )
      {
        std::cout << "    * " << c.first << " : " << c.second << " / " << std::endl;
      }
    }
  private:
    std::map<std::string,int> m_counters;    
  };
}


/** @class StoreCaloRecoChain StoreCaloRecoChain.h Extras/StoreCaloRecoChain.h
 * 
 * Clone & make persistent on a dedicated TES the low-level calo reco chain  associated to a given particle
 * [ Particle->ProtoParticle->CaloHypo ]  ==> [ CaloCluster->CaloDigits-> CaloADCs]
 *
 *  @author Olivier Deschamps
 *  @date   2016-12-06
 */
class StoreCaloRecoChain : public DaVinciAlgorithm {
public: 
  /// Standard constructor
  StoreCaloRecoChain( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~StoreCaloRecoChain  () = default;    ///< Destructor
  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution

protected:
  inline bool isPureNeutralCalo(const LHCb::Particle* P) const
  {
    LHCb::CaloParticle caloP(  (LHCb::Particle*) P );
    return caloP.isPureNeutralCalo();
  }
private:
  std::vector<const LHCb::Particle*> getCaloTree( const LHCb::Particle* p);
  // store calo objects
  bool storeCalo(const LHCb::Particle* p);
  bool storeHypo(const LHCb::CaloHypo* h);
  bool storeCluster(const LHCb::CaloCluster* c, LHCb::CaloClusters* clusters);
  const LHCb::CaloDigit* storeDigit(const LHCb::CaloDigit* d);
  bool storeADC(const LHCb::CaloDigit* d);
private:
  // calodigit checkers
  StoreCaloUtils::DigitFromCalo    m_spd{DeCalorimeterLocation::Spd};
  StoreCaloUtils::DigitFromCalo    m_prs{DeCalorimeterLocation::Prs};
  StoreCaloUtils::DigitFromCalo    m_ecal{DeCalorimeterLocation::Ecal};
  // adc providers
  ICaloDataProvider* m_edata = nullptr;
  ICaloDataProvider* m_pdata = nullptr;
  // TES pathes
  std::string m_locClusters;
  std::string m_locSplitClusters;
  std::string m_locEcalDigits;
  std::string m_locPrsDigits;
  std::string m_locSpdDigits;
  std::string m_locEcalAdcs;
  std::string m_locPrsAdcs;
  // TES containers
  LHCb::CaloClusters* m_clusters = nullptr;
  LHCb::CaloClusters* m_splitClusters   = nullptr;
  LHCb::CaloDigits*   m_edigits  = nullptr;
  LHCb::CaloDigits*   m_pdigits  = nullptr;
  LHCb::CaloDigits*   m_sdigits  = nullptr;
  LHCb::CaloAdcs*     m_eadcs    = nullptr;
  LHCb::CaloAdcs*     m_padcs    = nullptr;
  // properties
  std::string m_prefix;  
  bool m_mask = true;
  LHCb::CaloDigitStatus::Status m_status{LHCb::CaloDigitStatus::UseForEnergy|LHCb::CaloDigitStatus::UseForPosition|LHCb::CaloDigitStatus::UseForCovariance};
  StoreCaloUtils::Counter m_counter;  
  ICounterLevel* counterStat = nullptr;
};
#endif // STORECALORECOCHAIN_H
