#ifndef MUONOSWRAPPER_H
#define MUONOSWRAPPER_H 1

#include "TMVAWrapper.h"

namespace MyMuonOSSpace { class Read_muonMLPBNN; }

class MuonOSWrapper : public TMVAWrapper {
public:
	MuonOSWrapper(std::vector<std::string> &);
	~MuonOSWrapper();
	double GetMvaValue(std::vector<double> const &) override;

private:
	MyMuonOSSpace::Read_muonMLPBNN * reader;

};

#endif
