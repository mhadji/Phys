################################################################################
# Package: DaVinciDecayFinder
################################################################################
gaudi_subdir(DaVinciDecayFinder)

gaudi_depends_on_subdirs(Phys/DaVinciKernel)

find_package(ROOT)
find_package(Boost)
include_directories(SYSTEM ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})

gaudi_add_module(DaVinciDecayFinder
                 src/*.cpp
                 LINK_LIBRARIES DaVinciKernelLib)
