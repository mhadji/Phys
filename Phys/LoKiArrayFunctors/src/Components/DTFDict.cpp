#ifndef DTFDICTTOOL_H
#define DTFDICTTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IParticleDictTool.h"            // Interface used by this tool
#include "Kernel/IDecayTreeFit.h"
#include "LoKi/PhysTypes.h"
#include "LoKi/GetTools.h"
#include "LoKi/AuxDTFBase.h"

#include "GaudiKernel/Chrono.h"

#include "Kernel/PVSentry.h"

// std::lib
#include <map>
#include <string>

namespace LoKi
{
  namespace Hybrid
  {

    /** @class DTFDict
     *  Implementation of a Particle->Particle Transformation on
     *  a dictionary of Functors
     *  The particle for which the DictOfFunctors should be evaluated
     *  is transformed by applying the DecayTreeFitter
     *
     *  @author Sebastian Neubert
     *  @date   2013-07-10
     */
    class DTFDict : public GaudiTool, public LoKi::AuxDTFBase, virtual public IParticleDictTool{
    public:

      // ======================================================================
      /// Standard constructor
      DTFDict( const std::string& type,
                     const std::string& name,
                     const IInterface* parent)
        : GaudiTool ( type, name , parent ), AuxDTFBase(0)
      {
        declareInterface<IParticleDictTool>(this);

        declareProperty
          ( "Source" , m_sourcename ,
            "Type/Name for Dictionary Tool" );

        declareProperty
          ( "daughtersToConstrain",m_constraints,
            "list of particles to contrain");

        declareProperty
          ( "constrainToOriginVertex",m_usePV,
            "flag to switch on PV contraint");


      }

      // ======================================================================

      StatusCode initialize() override
      {
        StatusCode sc = GaudiTool::initialize() ;
        if ( sc.isFailure() ) {  return sc ; }               // RETURN
        // acquire the DictTool containing all the functors
        // that should be evaluated on the transformed particle
        m_source = tool<IParticleDictTool>(m_sourcename, this);
        if(m_source == NULL) return GaudiCommon<AlgTool>::Error("Unable to find the source DictTool " + m_sourcename , sc ) ;

        debug() << "Successfully initialized DTFDict" << endmsg;

        loadFitter("LoKi::DecayTreeFit/DTFDictFitter");
        setConstraint(m_constraints);

        return sc;
      }

      // ======================================================================

      StatusCode  fill ( const LHCb::Particle* p ,
                         IParticleDictTool::DICT& dict ) const override {

        Chrono chrono( chronoSvc(),name()+"::fill()" );
        if ( nullptr == p )
        {
          GaudiCommon<AlgTool>::Error ( "LHCb::Particle* points to NULL, dictionary will not be filled" ).ignore();
          return StatusCode::FAILURE; // RETURN
  }
        // 2. get primary vertex (if required)
        const LHCb::VertexBase* vertex = 0 ;
        if ( m_usePV )
        {
          vertex = bestVertex ( p ) ;
          if ( 0 == vertex )
          { GaudiCommon<AlgTool>::Warning ( "``Best vertex'' points to null, constraits will be disabled!" ).ignore() ; }
        }
        //

        applyConstraints(); // you need to do this for each individual fit!
        //printConstraints(std::cout);

        IDecayTreeFit* myfitter= fitter(); // from AuxDTFBase
        StatusCode sc = myfitter->fit(p, vertex);
        LHCb::Particle* prefitted = nullptr;
        if (sc.isFailure() )
        {
          // Hand an empty pointer to source and let it decide how to fill the
          // dictionary. This is needed because have too little information
          // on the structure of the dict at this point.
          GaudiCommon<AlgTool>::Warning ( "Error from IDecayTreeFit",sc ).ignore();
          dict.insert("DTF_CHI2",-1);
          dict.insert("DTF_NDOF",0);
          /// request the dictionary from the source DictTool
          m_source->fill(prefitted,dict).ignore();
        }
        else // everything went well
        {
          dict.insert("DTF_CHI2",myfitter->chi2());
          dict.insert("DTF_NDOF",myfitter->nDoF());
          LHCb::DecayTree tree = myfitter->fittedTree();
          // evaluate the dict of fucntors on the head of the refitted tree
          prefitted = tree.head();
          //lock the relation table Particle -> Primary Vertex
          //need to call getDesktop, since algorithm doesn't inherit from IDVAlgorithm
          //3rd argument ensures that relations of daughters are locked as well
          DaVinci::PVSentry sentry(this->getDesktop(),prefitted,true);
          /// request the dictionary from the source DictTool
          m_source->fill(prefitted,dict).ignore();
        }//sentry needs to go out of scope, so that destructor is called
        //tree.release();
        return StatusCode::SUCCESS;
      }

      // ======================================================================

      virtual ~DTFDict( ){}; ///< Destructor

      // ======================================================================

    protected:

      IParticleDictTool* m_source;
      std::string m_sourcename;

      std::vector<std::string> m_constraints;
      //std::map<std::string,std::string> m_options;
      bool m_usePV;

    };
  } //end namespace Hybrid
} //end namespace LoKi

DECLARE_TOOL_FACTORY(LoKi::Hybrid::DTFDict)

#endif
