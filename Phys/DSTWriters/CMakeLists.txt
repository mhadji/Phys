################################################################################
# Package: DSTWriters
################################################################################
gaudi_subdir(DSTWriters)

gaudi_depends_on_subdirs(GaudiAlg
                         GaudiConf)

find_package(Boost)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS})
                       
gaudi_add_module(DSTWriters
                 src/*.cpp
                 LINK_LIBRARIES GaudiAlgLib)

gaudi_install_python_modules()

