2017-07-21 Phys v24r0
=====================

Release for 2017 data and beyond
--------------------------------

This version is released on the master branch.
It is based on Gaudi v28r2, LHCb v43r0, Lbcom v21r0 and Rec v22r0 and uses LCG_88 with ROOT 6.08.06.

- Merge/synchronise 2017-patches into master.
  - This takes on board the following MRs: !148, !147, !144, !143, !142, !141, !138, !137, !136.
  - See merge request !152
- Modernize LoKiFitters
  - See merge request !150
- Modernize LoKiUtils
  - See merge request !149
- Explicitly call AuxFunBase constructor
  - See merge request !145
- Fix DVCommonBase compilation by making member mutable
  - See merge request !124
- Merge 2017-patches into master.
  - See merge request !132
